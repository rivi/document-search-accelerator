#ifndef ACCELERATOR_ACCELERATOR_HPP
#define ACCELERATOR_ACCELERATOR_HPP

#include <cmath>
#include <functional>
#include <set>
#include <string>
#include <vector>
#include <fstream>

#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/Dense>
#include <fmt/format.h>
#include <unordered_map>

struct StringReferenceEqual {
  bool operator()(const std::string &lhs, const std::string &rhs) const {
    return lhs == rhs;
  }
};

class SemanticSimilarity {
public:
  SemanticSimilarity(std::vector<std::string> words, double *vocabulary, int rows, int cols,
										 std::string centroids_file_path, std::string idf_file_path);
  ~SemanticSimilarity();
  void generate_local_centroids(std::uint8_t neighbourhood_size);
  std::vector<std::pair<std::string, double>> find_most_similar_words(const std::set<std::string> &query, int number);

private:
  double *_data;
  Eigen::Map<Eigen::MatrixXd> _vocabulary;
  std::vector<std::string> _words;
  std::unordered_map<std::reference_wrapper<const std::string>, int, std::hash<std::string>, StringReferenceEqual>
      _dictionary;
  std::string _centroids_file_path;
	std::string _idf_file_path;
  std::vector<double> _idfs;
  Eigen::MatrixXd _centroids;


  inline double _calculate_cosine_similarity(const Eigen::VectorXd &first_vector,
                                             const Eigen::VectorXd &second_vector) {
    return first_vector.dot(second_vector) / std::sqrt(first_vector.dot(first_vector) *
                                             second_vector.dot(second_vector));
  }


  inline double _calculate_cosine_similarity(const Eigen::VectorXd &first_vector,
                                             const Eigen::VectorXd &second_vector,
                                             const Eigen::VectorXd &first_vector_centroid) {
   return 1 - ((1 - _calculate_cosine_similarity(first_vector, second_vector)) -
   (1 - std::pow(_calculate_cosine_similarity(first_vector, first_vector_centroid), 10)));
  }

  inline double _calculate_sum(const Eigen::VectorXd &vector) {
    double sum = 0;
    for (int i = 0; i < _vocabulary.cols(); i++) {
      sum += _calculate_cosine_similarity(_vocabulary.col(i), vector, _centroids.col(i));
    }

    return sum;
  }
};

namespace Eigen {
template<class Matrix>
void write_binary(const std::string &filename, const Matrix &matrix) {
    std::ofstream out(filename, std::ios::out | std::ios::binary | std::ios::trunc);
    typename Matrix::Index rows = matrix.rows(), cols = matrix.cols();
    out.write(reinterpret_cast<char *>(&rows), sizeof(typename Matrix::Index));
    out.write(reinterpret_cast<char *>(&cols), sizeof(typename Matrix::Index));
    out.write(
      reinterpret_cast<const char *>(matrix.data()), rows * cols * static_cast<long>(sizeof(typename Matrix::Scalar))
    );
    out.close();
}

template<class Matrix>
void read_binary(const std::string &filename, Matrix &matrix) {
    std::ifstream in(filename, std::ios::in | std::ios::binary);
    typename Matrix::Index rows = 0, cols = 0;
    in.read(reinterpret_cast<char *>(&rows), sizeof(typename Matrix::Index));
    in.read(reinterpret_cast<char *>(&cols), sizeof(typename Matrix::Index));
    matrix.resize(rows, cols);
    in.read(reinterpret_cast<char *>(matrix.data()), rows * cols * static_cast<long>(sizeof(typename Matrix::Scalar)));
    in.close();
}
}

#endif // ACCELERATOR_ACCELERATOR_HPP

