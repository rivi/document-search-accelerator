cmake_minimum_required(VERSION 3.7)
project(DocumentSearchAccelerator)

add_definitions(-DEIGEN_DONT_PARALLELIZE)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -flto -std=c++1z")

if (CMAKE_CXX_COMPILER_ID MATCHES "Clang")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Weverything -Wno-c++98-compat -Wno-c++98-compat-pedantic --system-header-prefix=fmt/ --system-header-prefix=cpp_redis/ --system-header-prefix=tacopie/")
elseif (CMAKE_CXX_COMPILER_ID MATCHES "GNU")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra")
endif ()

set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -flto -fwhole-program")

include_directories(include)

add_subdirectory(lib)
add_subdirectory(progs)
