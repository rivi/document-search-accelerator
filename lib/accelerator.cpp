#include "accelerator.hpp"

#include <map>
#include <parallel/numeric>
#include <iostream>
#include <fstream>
#include <mutex>
#include <atomic>

#include <cereal/types/vector.hpp>
#include <cereal/archives/binary.hpp>

SemanticSimilarity::SemanticSimilarity(std::vector<std::string> words, double *vocabulary, int rows, int cols,
																			 std::string centroids_file_path, std::string idf_file_path)
    : _data{new double[static_cast<unsigned long>(rows) * static_cast<unsigned long>(cols)]},
      _vocabulary(_data, rows, cols), _words(std::move(words)), _centroids_file_path(std::move(centroids_file_path)),
      _idf_file_path(std::move(idf_file_path)) {
  std::copy(vocabulary, vocabulary + (rows * cols), _data);

  int index = 0;
  for (auto &word : _words) {
    _dictionary.emplace(std::cref(word), index++);
  }
}

SemanticSimilarity::~SemanticSimilarity() {
  delete[] _data;
}

void SemanticSimilarity::generate_local_centroids(std::uint8_t neighbourhood_size) {
  Eigen::MatrixXd centroids(_vocabulary.rows(), _vocabulary.cols());
  centroids.setZero();

  std::atomic<std::uint32_t> centroids_count{0};
  std::mutex print_mutex;
  #pragma omp parallel for
  for (int i = 0; i < _vocabulary.cols(); i++) {
    std::vector<std::pair<std::reference_wrapper<const std::string>, double>> similarities;
    similarities.reserve(static_cast<std::size_t>(_vocabulary.cols()));
    const auto vector = _vocabulary.col(i);

    for (int j = 0; j < _vocabulary.cols(); j++) {
      if (i == j) {
        similarities.emplace_back(std::cref(_words[static_cast<std::size_t>(j)]),
                                  std::numeric_limits<double>::min());
      }
      else {
        similarities.emplace_back(std::cref(_words[static_cast<std::size_t>(j)]),
                                  _calculate_cosine_similarity(vector, _vocabulary.col(j)));
      }
    }

    std::partial_sort(similarities.begin(), similarities.begin() + neighbourhood_size, similarities.end(),
                      [](const std::pair<std::reference_wrapper<const std::string>, double> &left,
                         const std::pair<std::reference_wrapper<const std::string>, double> &right) {
                        return left.second > right.second;
                      });

    for (std::uint8_t neighbour_number = 0; neighbour_number < neighbourhood_size; neighbour_number++) {
      centroids.col(i) += _vocabulary.col(_dictionary[similarities[neighbour_number].first]);
    }
    centroids.col(i) /= neighbourhood_size;
    centroids_count += 1;

    if (print_mutex.try_lock()) {
      std::cout << '\r' << centroids_count;
      print_mutex.unlock();
    }
  }
  std::cout << std::endl;

  Eigen::write_binary(_centroids_file_path, centroids);
}

std::vector<std::pair<std::string, double>>
SemanticSimilarity::find_most_similar_words(const std::set<std::string> &query, int number) {
  // TODO: move loading code to constructor to make this function re-invokable
  std::ifstream input_file(_idf_file_path);
  cereal::BinaryInputArchive input_archive(input_file);
  input_archive(_idfs);

  // Eigen::read_binary(_centroids_file_path, _centroids);

  std::vector<std::pair<std::reference_wrapper<const std::string>, double>> similarities;
  similarities.reserve(static_cast<std::size_t>(_vocabulary.cols()));

  std::vector<std::string> filtered_query;
  for (const auto &word : query) {
    if (_dictionary.count(word)) {
      filtered_query.emplace_back(word);
    }
    else {
      std::cerr << "Word `" << word << "` unknown" << std::endl;
    }
  }

  for (int i = 0; i < _vocabulary.cols(); i++) {
    if (query.count(_words[static_cast<std::size_t>(i)])) {
      similarities.emplace_back(std::cref(_words[static_cast<std::size_t>(i)]), 0.0);
    }
    else {
      auto vocabulary_word = _vocabulary.col(i);
      // auto centroid = _centroids.col(i);
      double similarity = 0.0;
      #pragma omp parallel for reduction (+:similarity)
      for (int word_n = 0; word_n < static_cast<int>(filtered_query.size()); word_n++) {
        auto word_index = _dictionary[filtered_query[static_cast<std::size_t>(word_n)]];
        similarity += _calculate_cosine_similarity(vocabulary_word, _vocabulary.col(word_index)) * std::pow(_idfs[i], 0.1) *
                      std::pow(_idfs[word_index], 0.2);
      }

      similarities.emplace_back(std::cref(_words[static_cast<std::size_t>(i)]), similarity);
    }
  }

  std::vector<std::pair<std::string, double>> top_similarities(static_cast<std::size_t>(number), {"", 0});
  std::partial_sort_copy(similarities.begin(), similarities.end(), top_similarities.begin(), top_similarities.end(),
                         [](const std::pair<std::reference_wrapper<const std::string>, double> &left,
                            const std::pair<std::reference_wrapper<const std::string>, double> &right) {
                           return left.second > right.second;
                         });

  return top_similarities;
}
