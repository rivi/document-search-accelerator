#include "trec_parser.hpp"

#include <algorithm>

#include <pugixml.hpp>

TextCleaner::TextCleaner() {
  _word.reserve(64);
}

std::string TextCleaner::clean_text(const std::string &text) {
  std::string clean_text;
  clean_text.reserve(text.size());

  for (const auto character : text) {
    if (not _is_punctuation(character) and character != ' ' and character != '(' and character != ')') {
      _word += static_cast<char>(::tolower(character));
    }
    else if (character == ' ') {
      if (std::find(_stop_words.begin(), _stop_words.end(), _word) == _stop_words.end()) {
        clean_text.append(_word);
        clean_text += ' ';
      }
      _word.clear();
    }
  }
  clean_text.append(_word);
  _word.clear();

  return clean_text;
}

std::string process_document(const std::string &path) {
  pugi::xml_document doc;
  pugi::xml_parse_result result = doc.load_file(path.c_str());

  if (result) {
    std::string document_content;

    auto abstract = doc.child("doc").child("abstract").text();
    auto body = doc.child("doc").child("body").text();

    if (not abstract.empty()) {
      document_content.append(abstract.as_string());
    }

    if (not body.empty()) {
      document_content.append(" ");
      document_content.append(body.as_string());
    }

    return document_content;
  }

  throw std::runtime_error(result.description());
}
