add_subdirectory(../libraries/pugixml pugixml)
include_directories(../libraries/pugixml/src)

add_executable(TrecMerge trec_merge.cpp trec_parser.hpp trec_parser.cpp)
target_link_libraries(TrecMerge stdc++fs pugixml)

include_directories(../libraries/cereal/include)

add_executable(IDFCalculator idf_calculator.cpp trec_parser.hpp trec_parser.cpp)
target_link_libraries(IDFCalculator stdc++fs pugixml ${Boost_LIBRARIES})

add_executable(Profile profile.cpp)
target_link_libraries(Profile accelerator)
