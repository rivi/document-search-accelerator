#ifndef TREC_PARSER_HPP
#define TREC_PARSER_HPP

#include <string>
#include <array>

class TextCleaner {
public:
  TextCleaner();

  std::string clean_text(const std::string &text);

private:
  const std::array<std::string, 3> _stop_words{{"a", "an", "the"}};
  std::string _word;

  inline bool _is_punctuation(char chr) {
    return chr != '-' and ::ispunct(chr);
  }
};

std::string process_document(const std::string &path);

#endif // TREC_PARSER_HPP
