#include <fstream>
#include <iostream>
#include <unordered_map>
#include <vector>
#include <cmath>
#include <unordered_set>
#include <experimental/filesystem>

#include <cereal/types/vector.hpp>
#include <cereal/archives/binary.hpp>

#include "trec_parser.hpp"

namespace fs = std::experimental::filesystem;

std::vector<std::string> split_document(const std::string &s) {
  std::vector<std::string> tokens;

  for(size_t p = 0, q = 0; p != s.npos; p = q) {
    tokens.emplace_back(s.substr(p + (p != 0), (q = s.find(' ', p + 1)) - p - (p != 0)));
  }

  return tokens;
}

int main(int argc, char *argv[]) {
  std::vector<double> numbers;
  
  
  if (argc == 4) {
    std::ifstream vocabulary_file(argv[1]);
    if (not vocabulary_file.is_open()) {
      std::cerr << "Vocabulary file does not exist" << std::endl;
      return 1;
    }

    std::ifstream merged_documents_file(argv[2]);
    if (not merged_documents_file.is_open()) {
      std::cerr << "Merged documents file does not exist" << std::endl;
      return 1;
    }

    std::vector<std::string> words;
    std::unordered_map<std::string, std::uint32_t> words_frequencies;
    std::string line;
    while (std::getline(vocabulary_file, line)) {
      std::istringstream line_stream(line);
      std::string word;
      line_stream >> word;

      words.emplace_back(std::move(word));
    }

    std::uint32_t documents_count = 0;
    while (std::getline(merged_documents_file, line)) {
      std::unordered_set<std::string> added_words;
      auto document_words = split_document(line);

      for (const auto &document_word : document_words) {
        if (not added_words.count(document_word)) {
          added_words.emplace(document_word);
          words_frequencies[document_word]++;
        }
      }

      documents_count++;
      std::cout << '\r' << documents_count;
    }
    std::cout << std::endl;

    std::vector<double> vocabulary_frequencies;
    words_frequencies.reserve(words.size());
    for (const auto &word : words) {
      if (words_frequencies[word] == 0) {
        vocabulary_frequencies.emplace_back(0.0);
      }
      else {
        vocabulary_frequencies.emplace_back(std::log(documents_count / static_cast<double>(words_frequencies[word])));
      }
    }

    std::ofstream output_file(argv[3]);
    cereal::BinaryOutputArchive output_archive(output_file);
    output_archive(vocabulary_frequencies);

    std::cout << "Documents count: " << documents_count << std::endl;

    return 0;
  }

  std::cerr << "Wrong number of arguments\n";
  std::cout << "Usage:\n";
  std::cout << "IDFCalculator <vocabulary> <documents directory> <output file>\n";

  return 1;
}
