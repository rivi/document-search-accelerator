#include <fstream>
#include <iostream>

#include <experimental/filesystem>

#include "trec_parser.hpp"

namespace fs = std::experimental::filesystem;

int main(int argc, char *argv[]) {
  if (argc == 4) {
    std::string command(argv[1]);

    if (command == "clean") {
      std::ifstream input_file(argv[2]);
      if (not input_file.is_open()) {
        std::cerr << "File does not exist" << std::endl;
        return 1;
      }

      std::ofstream output_file(argv[3]);

      TextCleaner cleaner;
      std::string line;
      while (std::getline(input_file, line)) {
        output_file << cleaner.clean_text(line) << '\n';
        line.clear();
      }

      return 0;
    }
    else if (command == "merge") {
      std::ofstream output_file(argv[3]);
      if (not output_file) {
        std::cerr << "Error opening file" << std::endl;
        return 1;
      }

      for (auto &first_level_dir : fs::directory_iterator(argv[2])) {
        for (auto &second_level_dir : fs::directory_iterator(first_level_dir)) {
          for (const auto &file : fs::directory_iterator(second_level_dir)) {
            try {
              output_file << process_document(file.path()) << '\n';
            }
            catch (std::runtime_error &exception) {
              std::cerr << exception.what() << std::endl;
            }
          }
        }
      }

      return 0;
    }
    else if (command == "merge_and_clean") {
      std::ofstream output_file(argv[3]);
      if (not output_file) {
        std::cerr << "Error opening file" << std::endl;
        return 1;
      }

      TextCleaner cleaner;
      for (auto &first_level_dir : fs::directory_iterator(argv[2])) {
        for (auto &second_level_dir : fs::directory_iterator(first_level_dir)) {
          for (const auto &file : fs::directory_iterator(second_level_dir)) {
            try {
              output_file << cleaner.clean_text(process_document(file.path())) << '\n';
            }
            catch (std::runtime_error &exception) {
              std::cerr << exception.what() << std::endl;
            }
          }
        }
      }

      return 0;
    }

    std::cerr << "Invalid command: " << argv[1] << std::endl;
    return 1;
  }

  std::cerr << "Wrong number of arguments" << std::endl;
  std::cout << "Usage:\n";
  std::cout << "TrecMerge clean <input file> <output file>";
  std::cout << "TrecMerge merge <input file> <output file>";
  std::cout << "TrecMerge merge_and_clean <input file> <output file>";

  return 1;
}
